package Lab3;

import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.List;
import static org.testng.AssertJUnit.assertEquals;
public class MyTest {
    @Test
    public void testForSearchbyName(){
        Item it = new Item();
        it.setName("kitchen curbstone");
        it.setType("kitchen");
        it.setPrice(700);
        List<Item> item=new ArrayList<>();
        item.add(it);
        Supply supp = new Supply();
        supp.setOrder(item);
        supp.setName("kitchen");
        supp.setTotalPrice(700);
        assertEquals(supp.searchbyName("kitchen curbstone"),item);
    }
    @Test
    public void testForSearchbyType(){
        Item it = new Item();
        it.setName("kitchen curbstone");
        it.setType("kitchen");
        it.setPrice(700);
        List<Item> item=new ArrayList<>();
        item.add(it);
        Supply supply = new Supply();
        supply.setOrder(item);
        supply.setName("kitchen");
        supply.setTotalPrice(700);
        assertEquals(supply.searchbyType("kitchen"),item);
    }



}
