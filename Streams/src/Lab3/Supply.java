package Lab3;

import java.util.List;
import java.util.stream.Collectors;

public class Supply {
    private double totalPrice;

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    private String complectTitle;

    public String getName() {
        return complectTitle;
    }

    public void setName(String name) {
        this.complectTitle = name;
    }

    private List<Item> order;

    public List<Item> getOrder() {
        return order;
    }

    public void setOrder(List<Item> order) {
        this.order = order;
    }

    public List<Item> searchbyName(String name){
        return order.stream().filter((elem)-> name.equals(elem.getName())).collect(Collectors.toList());
    }

    public List<Item> searchbyType(String type){
        return order.stream().filter((elem)-> type.equals(elem.getType())).collect(Collectors.toList());
    }


}

