package Lab6;
import java.sql.*;
import java.util.Date;

public class Sql {
    Connection conn = null;

    private Connection connect() throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
        String userName = "root";
        String password = "12345";
        String url = "jdbc:mysql://localhost/SupplyDepartment";
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        conn = DriverManager.getConnection(url, userName, password);
        System.out.println("Database is connected");
        return conn;
    }

    private void disconnect() throws SQLException {
        if (conn != null) {
            conn.close();
            System.out.println("Database is off");

        }
    }

    Stocks SearchbyName(String name) throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        String search = "SELECT kod_stocks,name,price FROM stocks WHERE name=?;";
        conn = connect();
        try (PreparedStatement sn = conn.prepareStatement(search)) {
            sn.setString(1,name);
            ResultSet rs = sn.executeQuery();
            Stocks s = new Stocks();
            if (rs != null) {
                s.setKod_stocks(rs.getInt("kod_stocks"));
                s.setName(rs.getString("name"));
                s.setPrice(rs.getDouble("price"));
            }
            return s;
        } finally {
            this.disconnect();
        }
    }

    boolean addStocks(Stocks t) throws SQLException, IllegalAccessException, InstantiationException, ClassNotFoundException {
        String insert = "INSERT INTO stocks (name,price) VALUES(?, ?);";
        conn = connect();
        conn.setAutoCommit(false);
        try (PreparedStatement sn = conn.prepareStatement(insert)) {
            sn.setString(1, t.getName());
            sn.setDouble(2, t.getPrice());
            sn.executeUpdate();
            conn.commit();
            return sn.executeUpdate()>0;

        } finally {
            this.disconnect();
        }

    }
}






