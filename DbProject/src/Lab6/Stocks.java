package Lab6;

import java.util.Date;

public class Stocks {
    int kod_stocks;
    String name;
    double price;

    @Override
    public String toString() {
        return "Stocks{" +
                "kod_stocks=" + kod_stocks +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    public int getKod_stocks() {
        return kod_stocks;
    }

    public void setKod_stocks(int kod_stocks) {
        this.kod_stocks = kod_stocks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
