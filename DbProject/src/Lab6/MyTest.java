package Lab6;

import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.SQLException;

import static org.testng.Assert.assertTrue;
import static org.testng.AssertJUnit.assertEquals;

public class MyTest {
    @Test
    public void testForSearchbyName() throws IllegalAccessException, ClassNotFoundException, InstantiationException, SQLException {
        Stocks s = new Stocks();
        s.setKod_stocks(1);
        s.setName("wood");
        s.setPrice(1600);
        Sql l = new Sql();
        assertEquals(l.SearchbyName("wood"), s);
    }

    @Test
    public void testAddStocks() throws SQLException, IllegalAccessException, ClassNotFoundException, InstantiationException {
        Sql l = new Sql();
        Stocks s = new Stocks();
        s.setKod_stocks(9);
        s.setName("metal");
        s.setPrice(1500);
        assertTrue(l.addStocks(s));
    }

        }



