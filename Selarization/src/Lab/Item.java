package Lab;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Item {
    private static String type;

    static String getType() {
        return type;
    }

    void setType(String type) {
        this.type = type;
    }

    private static String name;

    static String getName() {
        return name;
    }

    void setName(String name) {
        this.name = name;
    }

    private static double price;

    static double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean validateType() {
        String regx = "(^[А-Я]{1}[а-я]{1,14})";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(String.valueOf(this.getType()));
        return matcher.find();
    }

    public boolean validateName() {
        String regx = "(^[А-Я]{1}[а-я]{1,14} [А-Я]{1}[а-я]{1,14})";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(String.valueOf(this.getName()));
        return matcher.find();
    }

    public boolean validatePrice() {
        String regx = "(^(?!0.*$)([0-9]{1,3}(,[0-9]{3})?(,[0-9]{3})?(\\.[0-9]{2})?)$)";
        Pattern pattern = Pattern.compile(regx, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(String.valueOf(this.getPrice()));
        return matcher.find();
    }

    public void isValid() {
        if (!this.validateType()) {
            throw new ArithmeticException("check the correctness of type");
        }
        if (!this.validateName()) {
            throw new ArithmeticException("check the correctness of name");
        }
        if (!this.validatePrice()) {
            throw new ArithmeticException("check the correctness of price");
        }

    }
}

