package Lab;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;

public class Xml {
    public class toXML implements Serializer {
        @Override
        public void serializeItem(Item item, String path)  {
            item.isValid();
            try {
                JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                jaxbMarshaller.marshal(item, new File(path));

            }
            catch (JAXBException e) {
                e.printStackTrace();
            }}

        @Override
        public Item deserializeItem(String path) throws IOException, JAXBException {
            JAXBContext jaxbContext = JAXBContext.newInstance(Item.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            return (Item) jaxbUnmarshaller.unmarshal(new File(path));
        }
    }

}
