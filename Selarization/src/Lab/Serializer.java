package Lab;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public interface Serializer {
    Item deserializeItem(String path) throws IOException, JAXBException;

    void serializeItem(Item objectsToSerial, String path) throws IOException, JAXBException;
}
