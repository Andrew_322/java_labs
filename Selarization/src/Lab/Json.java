package Lab;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.xml.bind.JAXBException;
import java.io.*;

public class Json implements Serializer {
    @Override
    public void serializeItem(Item item, String path) throws IOException {
        item.isValid();
        File file = new File(path);
        JSONObject object = new JSONObject();
        object.put("Type",Item.getType());
        object.put("Name",Item.getName());
        object.put("Price",Item.getPrice());
        JSONArray array = new JSONArray();
        array.add(object);
        FileWriter fw = null;
        fw = new FileWriter(file);
        fw.write(array.toString());
        fw.close();
    }

    @Override
    public Item deserializeItem(String path) throws IOException, JAXBException {
        File fl = new File(path);
        FileInputStream fin = new FileInputStream(fl);
        BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
        Item item = new Item();
        item.setType(reader.readLine());
        item.setName(reader.readLine());
        item.setPrice(Double.parseDouble(reader.readLine()));
        return item;
    }
}
