package Lab;
import javax.xml.bind.JAXBException;
import java.io.*;

public class Txt implements Serializer {
    @Override
    public void serializeItem(Item item, String path) throws IOException {
        item.isValid();
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        writer.write(item.toString());
        if (writer != null)
            writer.close();
    }

    @Override
    public Item deserializeItem(String path) throws IOException, JAXBException {
        BufferedReader br = new BufferedReader(new FileReader(new File((path))));
        Item item = new Item();
        item.setType(br.readLine());
        item.setName(br.readLine());
        item.setPrice(Double.parseDouble(br.readLine()));
        return item;
    }
}
