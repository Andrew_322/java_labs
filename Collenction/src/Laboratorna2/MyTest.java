package Laboratorna2;

import org.testng.annotations.Test;
import java.util.ArrayList;
import java.util.List;
import static org.testng.AssertJUnit.assertEquals;
public class MyTest {
    @Test
    public void testForSearchbyName(){
        Item it = new Item();
        it.setName("kitchen curbstone");
        it.setType("kitchen");
        it.setPrice(700);
        List<Item> item=new ArrayList<>();
        item.add(it);
        Department department = new Department();
        department.setOrder(item);
        department.setName("kitchen");
        department.setTotalPrice(700);
        assertEquals(department.searchbyName("kitchen curbstone"),it.getType());
        }
    @Test
    public void testForSearchbyType(){
        Item it = new Item();
        it.setName("kitchen curbstone");
        it.setType("kitchen");
        it.setPrice(700);
        List<Item> item=new ArrayList<>();
        item.add(it);
        Department department = new Department();
        department.setOrder(item);
        department.setName("kitchen");
        department.setTotalPrice(700);
        assertEquals(department.searchbyType("kitchen"),it.getName());
    }



}
