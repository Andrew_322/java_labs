package Laboratorna2;

import java.util.List;
import java.util.Objects;

public class Department {
    private double totalPrice;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department that = (Department) o;
        return Double.compare(that.getTotalPrice(), getTotalPrice()) == 0 &&
                Objects.equals(complectTitle, that.complectTitle) &&
                Objects.equals(getOrder(), that.getOrder());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTotalPrice(), complectTitle, getOrder());
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    private String complectTitle;

    public String getName() {
        return complectTitle;
    }

    public void setName(String name) {
        this.complectTitle = name;
    }

    private List<Item> order;

    public List<Item> getOrder() {
        return order;
    }

    public void setOrder(List<Item> order) {
        this.order = order;
    }

    public String searchbyName(String name){
        for(Item elem : order){
            if(name.equals(elem.getName())) {
                return elem.getType();
            }
        }
        Item it = new Item();
        return it.getType();
    }

    public String searchbyType(String type){
        for(Item elem : order) {
                if (type.equals(elem.getType())) {
                    return elem.getName();
                }
        }
        Item it = new Item();
        return it.getName();
    }


}

